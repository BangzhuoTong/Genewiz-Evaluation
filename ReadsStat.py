import pandas as pd
import os

rmHostStatFiles = []
for rootdir,subdir,files in os.walk('/projects/Engineering/pipeline_output/AWS/PipelineJob_32_20210422/kneaddata/'):
    for file in files:
        if file.endswith('kneaddata_read_counts.txt'):
            print(file)
            print(rootdir)
            statfile = os.path.join(rootdir,file)
            rmHostStatFiles.append(statfile)

dflist = []
for file in rmHostStatFiles:
    df = pd.read_csv(file, sep = '\t',index_col = False, header=0)
    dflist.append(df)

integrated_df = pd.concat(dflist)
integrated_df.to_csv('rmHostStat.xls', sep='\t', header=True, index=True)
