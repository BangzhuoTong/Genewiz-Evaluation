---
title: "供体筛选"
author: "tbz"
date: "2021/4/26"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.width=20,fig.height=10,echo=FALSE,warning=FALSE,message=FALSE,echo=FALSE)
library(reticulate)
library(dplyr)
library(xviz)
library(VennDiagram)
library(ggplot2)
library(metamicrobiomeR)
library(readxl)
library(stringi)
library(gridExtra)
library(png)
library(downloader)
library(curatedMetagenomicData)
source("/home/xuxiaomin/R/repository/metaphlan2phyloseq.R")
library(ComplexHeatmap)
library(magrittr)
library(tidyverse)
library(corrr)
library(randomForest)
library(plyr)


filter_low_abun <- function(data, cutoff){
    for(i in 1:ncol(data)){
      for(j in 1:nrow(data)){
        cur = data[j,i]
        data[j,i] = ifelse(data[j,i]<cutoff,0,cur)
    }
      data[,i] = data[,i]/sum(data[,i])
  }
  return(data)
}

rescale <- function(data){
  for(i in 1:ncol(data)){
    data[,i] = data[,i]/sum(data[,i])
  }
  return(data)
}
```

# 1. PCOA
```{r PCOA}
Donordata <- read_xlsx('/Xbiome1/tongbangzhuo/Project/NewTech/Donor_selection/GD00909001/Species_Abundance.xlsx', sheet = 1) %>% as.data.frame()
colnames(Donordata)[1] <- 'seqid'
colnames(Donordata) %<>% str_remove('Species__')
Donordata %<>% filter(ProductionID == "M15"  | ProductionID == "M16" | ProductionID == "M17" | ProductionID == "F18" | ProductionID == "M19") %>% select(!c(CollectionDate, ProductionID)) %>% column_to_rownames('seqid')
PatientProfile <- read.table('/Xbiome1/tongbangzhuo/Project/NewTech/Donor_selection/GD00909001/4886_metaphlan_bugs_list.tsv', header = FALSE, sep = "\t")
colnames(PatientProfile)[c(1,2)] <- c('ID','4886')
PatientProfile %<>% filter(str_detect(ID, 's__'), !str_detect(ID, 't__')) %>%  mutate(ID=str_remove(ID, '.*s__')) %>% column_to_rownames('ID') %>% filter_low_abun(., 0.01) %>% filter(.[,] > 0) 

temp_PatientProfile <- PatientProfile
for (species in colnames(Donordata)){
  if (species %in% rownames(temp_PatientProfile)){
    next()
  }
  else{
    temp_PatientProfile[species, ] <- 0
  }
}

combined_profile <- merge(temp_PatientProfile %>% rownames_to_column('Species'), Donordata %>% t() %>% as.data.frame() %>% rownames_to_column('Species'), by = 'Species')
species_vec <- combined_profile$Species
combined_profile %<>% apply(., 2, as.numeric)
rownames(combined_profile) <- species_vec
combined_profile %<>% .[,-1] %>% filter_low_abun(., 0.0001) %>% as.data.frame() %>% filter(rowSums(.) > 0)

metadata <- read.table('/Xbiome1/tongbangzhuo/Project/NewTech/Donor_selection/GD00909001/Donor_metadata.xls', sep ="\t", header = TRUE)

##Stool
stool_donor_id <- metadata %>% filter(Sampletype == 'Stool') %>% .$SeqID %>% as.character()
stool_combined_profile <- combined_profile %>% select(c('4886',stool_donor_id))
stool_meta <- metadata %>% filter(Sampletype == 'Stool' | Sampletype == 'NF') %>% column_to_rownames('SeqID')
Stool_phylo <- phyloseq(otu_table(stool_combined_profile, taxa_are_rows = TRUE), sample_data(stool_meta))
Stool_pcoa <- plot_beta_diversity(phyloseq =Stool_phylo, feature = 'Sample', add_pc1 = TRUE, add_pc2 =  TRUE)

##Capsule
capsule_donor_id <- metadata %>% filter(Sampletype == 'Capsule') %>% .$SeqID %>% as.character()
capsule_combined_profile <- combined_profile %>% select(c('4886',capsule_donor_id))
capsule_meta <- metadata %>% filter(Sampletype == 'Capsule' | Sampletype == 'NF') %>% column_to_rownames('SeqID')
Capsule_phylo <- phyloseq(otu_table(capsule_combined_profile, taxa_are_rows = TRUE), sample_data(capsule_meta))
capsule_pcoa <- plot_beta_diversity(phyloseq =Capsule_phylo, feature = 'Sample', add_pc1 = TRUE, add_pc2 =  TRUE)
```

# 2. Donor与患者Bray-Curtis距离
```{r Bray-Curtis}
stool_Dist <- vegan::vegdist(stool_combined_profile %>% t(), method = "bray" , binary=TRUE) %>% as.matrix()  %>% as.data.frame() %>% select('4886')
stool_Bray_Dist <- stool_Dist %>% rownames_to_column('ID') %>% mutate(Sample = metadata %>% column_to_rownames('SeqID') %>% .[rownames(stool_Dist),] %>% .$Sample, Type = 'Stool')

cap_Dist <- vegan::vegdist(capsule_combined_profile %>% t(), method = "bray" , binary=TRUE) %>% as.matrix()  %>% as.data.frame() %>% select('4886')
cap_Bray_Dist <- cap_Dist %>% rownames_to_column('ID') %>% mutate(Sample = metadata %>% column_to_rownames('SeqID') %>% .[rownames(cap_Dist),] %>% .$Sample, Type = 'Capsule')

distance_data <- rbind(stool_Bray_Dist, cap_Bray_Dist) %>% filter(ID != '4886') %>% filter(Sample == 'M19' |Sample == 'M16')
colnames(distance_data)[2] <- 'BC Distance'

ggplot(distance_data, aes(x=Sample, y=`BC Distance`)) +
  geom_boxplot() + geom_point(aes(col = Type), size = 5)  + theme_minimal(base_size = 25)


cap_distance_data <- distance_data %>% filter(Type == 'Capsule')
ggplot(cap_distance_data, aes(x=Sample, y=`BC Distance`)) +
  geom_boxplot() + geom_point(size = 5)  + theme_minimal(base_size = 25)
```

# 3. Donor M19,M16 疾病相关菌丰度情况
```{r 疾病相关菌, fig.height = 10, fig.width = 15}
disease_bacteria <- read.table('diesease_bacteria.xls', header = FALSE, sep = "\t") %>% unlist() %>% as.vector() %>% str_replace_all(' ', '_') %>% unique()
disease_bacteria_info <- read.table('diesease_bacteria_info.xls', header = TRUE, sep = "\t")
disease_bacteria_info$microbe_name %<>%  str_replace_all(' ', '_')
disease_bacteria_info <- disease_bacteria_info[str_detect(disease_bacteria_info$microbe_name, '_'),] 
rownames(disease_bacteria_info) <- NULL


disease_bacteria <- disease_bacteria[str_detect(disease_bacteria, '_')]
#stool_disease_related_profile <- stool_combined_profile[intersect(disease_bacteria, rownames(stool_combined_profile)), stool_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M16' | Sample == "M19" | Sample == 'GD00909001') %>% .$SeqID ]
stool_disease_related_profile <- stool_combined_profile[intersect(disease_bacteria, rownames(stool_combined_profile)), stool_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M16' | Sample == "M19" ) %>% .$SeqID ]
#capsule_disease_related_profile <- capsule_combined_profile[intersect(disease_bacteria, rownames(capsule_combined_profile)), capsule_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M16' | Sample == "M19" | Sample == 'GD00909001') %>% .$SeqID]
capsule_disease_related_profile <- capsule_combined_profile[intersect(disease_bacteria, rownames(capsule_combined_profile)), capsule_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M16' | Sample == "M19") %>% .$SeqID]

disease_stool_obj <- phyloseq(otu_table(stool_disease_related_profile, taxa_are_rows = TRUE), sample_data(stool_meta))
plot_stacked_bar(otu_table = stool_disease_related_profile %>% t(), metadata =  stool_meta , collapse = FALSE) + facet_wrap(vars(Sample), scales = 'free')

disease_capsule_obj <- phyloseq(otu_table(capsule_disease_related_profile, taxa_are_rows = TRUE), sample_data(capsule_meta))
plot_stacked_bar(otu_table = capsule_disease_related_profile %>% t(), metadata =  capsule_meta , collapse = FALSE) + facet_wrap(vars(Sample), scales = 'free')


```

```{r 疾病相关菌Heatmap, fig.height = 10, fig.width = 10}
## Capsule有益菌热图
Helpful_capsule_spe <- disease_bacteria_info %>% filter(evidence == 'decrease') %>% .$microbe_name %>%  intersect(., intersect(disease_bacteria, rownames(capsule_combined_profile)))


info <- capsule_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M19' | Sample == 'M16') %>% column_to_rownames('SeqID') %>% select(Sample)
colnames(info) <- 'Donor'
anno <- HeatmapAnnotation(df = info ,name = "Group",
                         col = list(Donor = c( 'M16' = 'Khaki' , 'M19' = 'purple')))

Heatmap(capsule_disease_related_profile[Helpful_capsule_spe, ] %>% as.matrix(),
                    top_annotation = anno,
                    column_names_rot = 90,
                    name = "Species_abundance",
                    col = colorRamp2(c(0,0.0004092696,0.5), c("white","orange","red")),
                    row_names_gp = gpar(fontsize = 10),
                    cluster_rows = FALSE,
                    cluster_columns = FALSE,
                    column_title_side =  'top',
                    show_heatmap_legend = TRUE)

## Capsule有害菌热图
Harmful_capsule_spe <- disease_bacteria_info %>% filter(evidence == 'increase') %>% .$microbe_name %>%  intersect(., intersect(disease_bacteria, rownames(capsule_combined_profile)))

info <- capsule_meta %>% rownames_to_column('SeqID') %>% filter(Sample == 'M19' | Sample == 'M16') %>% column_to_rownames('SeqID') %>% select(Sample)
colnames(info) <- 'Donor'
anno <- HeatmapAnnotation(df = info ,name = "Group",
                         col = list(Donor = c( 'M16' = 'Khaki' , 'M19' = 'purple')))

Heatmap(capsule_disease_related_profile[Harmful_capsule_spe,] %>% as.matrix(),
                    top_annotation = anno,
                    column_names_rot = 90,
                    name = "Species_abundance",
                    col = colorRamp2(c(0,0.0004092696,0.4), c("white","orange","red")),
                    row_names_gp = gpar(fontsize = 10),
                    cluster_rows = FALSE,
                    cluster_columns = FALSE,
                    show_heatmap_legend = TRUE)


```